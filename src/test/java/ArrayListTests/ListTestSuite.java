package ArrayListTests;

import ListArray.MyArrayList;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kirif on 27.07.2017.
 */
public class ListTestSuite {
    @Test
    void sizeTest(){
        List list = new MyArrayList();
        Assert.assertEquals(0,list.size(),"Array is not empty" );
    }

    @Test
    void addTest(){
        List list = new MyArrayList();
        list.add(4);
        list.add("Java");
        list.add("PHP");
        Assert.assertEquals(3,list.size(), "Not added elements");
    }
    @Test
    void addArrayExpansionTest(){
        List list = new MyArrayList();
        for (int i = 0; i < 11; i++) {
            list.add(14);
        }
        Assert.assertEquals(11, list.size(), "Incorrect list size");
    }

    @Test
    void removeTest(){
        List list = new MyArrayList();
        list.add("Java");
        list.add("PHP");
        list.add("Javascript");
        Assert.assertTrue(list.remove("PHP"), "Element was not removed");
        Assert.assertEquals(2,list.size(), "Incorrect size after remove 1 element");
        for (Object o: list.toArray()){
            System.out.println(o);
        }
    }

    @Test
    void removeNoExistTest(){
        List list = new MyArrayList();
        list.add("Java");
        list.add("PHP");
        list.add("Javascript");
        Assert.assertTrue(list.remove("Jopascript"), "there is no such element in list");
        Assert.assertEquals(2,list.size(), "Incorrect size after remove nonexistent element");
        for (Object o: list.toArray()){
            System.out.println(o);
        }
    }

    @Test
    void containsTest(){
        List list = new MyArrayList();
        list.add("PHP");
        list.add("JAVA");
        list.add("Javascript");
        Assert.assertTrue(list.contains("JAVA"),"list doesn't contains element");
        Assert.assertEquals(3, list.size(),"Incorrect list size");
        for (Object o: list.toArray()){
            System.out.println(o);
        }
    }

    @Test
    void getTest(){
        List list = new MyArrayList();
        list.add("Java");
        Assert.assertEquals("Java", list.get(0),"There is no element in the list");
        for (Object o: list.toArray()){
            System.out.println(o);
        }
    }

    @Test
    void setTest(){
        List list = new MyArrayList();
        list.add("Java");
        list.add("Javascript");
        list.set(1,"PHP");
        Assert.assertEquals("PHP",list.get(1),"Element doesn't set to the list");
        for (Object o: list.toArray()){
            System.out.println(o);
        }
    }

    @Test
    void indexTest(){
        List list = new MyArrayList();
        list.add("Java");
        list.add("PHP");
        Assert.assertEquals(1,list.indexOf("PHP"),"Index doesn`t exist in the list");
        for (Object o: list.toArray()){
            System.out.println(o);
        }
    }

    @Test
    void addAllTest() {
        List listOne = new MyArrayList();
        List listTwo = new MyArrayList();
        listOne.add("Javascript");
        listOne.add("PHP");
        listTwo.add("Java");
        listTwo.add("Scala");
        listTwo.addAll(listOne);
        Assert.assertEquals(5, listTwo.size(), "Elements listOne not added to listTwo");
        for (Object o : listTwo.toArray()) {
            System.out.println(o);
        }
    }

    @Test
    void iteratorTest() {
        List list = new MyArrayList();
        list.add("Javascript");
        list.add("PHP");
        Iterator iterator = list.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
        System.out.println("Test iterator true");
    }
}

