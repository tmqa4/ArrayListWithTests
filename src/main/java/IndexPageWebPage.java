import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by kirif on 14.08.2017.
 */
public class IndexPageWebPage {

    @FindBy (xpath = "//*[@id='menu-signin-block']/preceding-sibling::li[2]//*[@id='menu-favorites']")
    public WebElement collectionLink;

    @FindBy(xpath = "//*[contains(@class, 'js-sub-menu-1')]/ul/li")
    public List<WebElement> navigationTypesList;

    @FindBy (xpath = "//a[contains(@id, 'nm-cat-fashion-beauty')]//span[contains(@class, 'title_text')]")
    public WebElement fashionTypeLink;

    @FindBy (xpath = "//*[@id='tab-new']//ul/li[3]//*[contains(@class, 'tm-icon icon-heart-empty')]")
    public WebElement addToCollectionLink;

}
