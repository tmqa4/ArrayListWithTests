package ListArray;

import java.util.*;

/**
 * Created by kirif on 27.07.2017.
 */
public class MyArrayList implements List{

    private Object[] array;
    private int size;
    private int capacity = 10;

    public MyArrayList() {
        array = new Object[capacity];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public void clear() {
        size = 0;
        capacity = 10;
        array = new Object[capacity];
    }

    @Override
    public boolean add(Object obj) {

        if (obj == null) return false;
        if (size < capacity) {
            array[size] = obj;
        } else {
            capacity += 10;
            Object[] arrBig = new Object[capacity];
            for (int i = 0; i < size; i++) {
                arrBig[i] = array[i];
            }
            arrBig[size + 1] = obj;
            array = arrBig;
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (o == null) {
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (array[i].equals(o)) {
                for (int j = i; j < size; j++) {
                    array[j] = array[j + 1];
                }
            }
            size--;
            return true;
        }
        return false;
    }

    @Override
    public Object get(int index) {
        if (index < size) {
            return array[index];
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    @Override
    public Object set(int index, Object element) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        Object oldValue = array[index];
        array[index] = element;
        return oldValue;
    }

    @Override
    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++)
                if (array[i] == null)
                    return i;
        } else {
            for (int i = 0; i < size; i++)
                if (o.equals(array[i]))
                    return i;
        }
        return -1;
    }

    public Object[] toArray() {
        return array;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) >= 0;
    }

    @Override
    public Iterator<Object> iterator() {
        return null;
    }

    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        Iterator iterator= c.iterator();
        while (iterator.hasNext()) {
            add(iterator.next());
        }
        return true;
    }

    public boolean addAll(int index, Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public void add(int index, Object element) {

    }

    @Override
    public Object remove(int index) {
        return null;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<Object> listIterator() {
        return null;
    }

    @Override
    public ListIterator<Object> listIterator(int index) {
        return null;
    }

    @Override
    public List<Object> subList(int fromIndex, int toIndex) {
        return null;
    }

    private class MyIterator implements Iterator {

        int cursor;
        
        @Override
        public boolean hasNext() {
            return cursor!=size;
        }
        @Override
        public Object next() {
            return hasNext() ? get(cursor++) : null;
        }

        public void remove() {
            throw new UnsupportedOperationException("remove() method is not supported");
        }
    }
}
